from django.db import models

# Create your models here.
class Blog(models.Model):
    #タイトル
    title = models.CharField(max_length=250)
    #発行日
    pub_date = models.DateTimeField()
    #適当な画像
    image = models.ImageField(upload_to='media/')
    #記事本文
    body = models.TextField()
