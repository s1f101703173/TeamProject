from django.shortcuts import render
from .models import Blog
# Create your views here.
def home(request):
    blogs=Blog.objects.order_by('pub_date')
    return render(request, 'blogs/home.html',{'blogs':blogs})
